use bevy::prelude::*;
use wasm_bindgen::prelude::*;
mod camera;
mod kart;
mod log;
mod racetrack;
mod scene;
mod ui;

#[wasm_bindgen(start)]
pub fn main() -> Result<(), JsValue> {
    ui::close_loader().or(Err(JsValue::from_str("Failed to close loader")))?;
    let window_dimensions = ui::get_window_dimensions();

    App::new()
        .add_plugins(DefaultPlugins.set(WindowPlugin {
            window: WindowDescriptor {
                title: "Bevy".to_string(),
                width: window_dimensions.0,
                height: window_dimensions.1,
                ..Default::default()
            },
            ..Default::default()
        }))
        .insert_resource(ClearColor(Color::rgb(0.0, 0.0, 0.0)))
        .add_startup_system(scene::setup_scene)
        .add_startup_system(kart::setup_kart)
        .add_startup_system(camera::setup_camera)
        .add_startup_system(racetrack::add_racetrack_to_scene)
        .add_system(kart::move_kart_system)
        .add_system(camera::third_person_camera_system)
        .run();
    Ok(())
}
