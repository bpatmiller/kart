use bevy::prelude::*;

/// set up lighting and background components
pub fn setup_scene(mut commands: Commands) {
    // Global light
    commands.spawn(DirectionalLightBundle {
        transform: Transform::from_xyz(4.0, 8.0, 4.0),
        directional_light: DirectionalLight {
            color: Color::rgb(1.0, 1.0, 1.0),
            illuminance: 100000.0,
            shadows_enabled: true,
            ..Default::default()
        },
        ..Default::default()
    });

    // Point light
    commands.spawn(PointLightBundle {
        transform: Transform::from_xyz(1.5, 5.0, -1.0),
        point_light: PointLight {
            color: Color::rgb(1.0, 1.0, 1.0),
            range: 100.0,
            ..Default::default()
        },
        ..Default::default()
    });
}
