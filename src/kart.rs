use crate::log::log;
use bevy::prelude::*;

#[derive(Component)]
pub struct Kart;

pub fn setup_kart(mut commands: Commands, asset_server: Res<AssetServer>) {
    // dynamically read the size of the gltf file and scale the kart accordingly

    log("Loading kart model...");

    let gltf_handle = asset_server.load("models/classic_muscle_car.glb#Scene0");
    let scale = 0.25;

    commands
        .spawn(SceneBundle {
            scene: gltf_handle,
            transform: Transform::from_xyz(0.0, 0.5, 0.0).with_scale(Vec3 {
                x: scale,
                y: scale,
                z: scale,
            }),
            ..Default::default()
        })
        .insert(Kart);
}

pub fn move_kart_system(
    keyboard_input: Res<Input<KeyCode>>,
    mut query: Query<(&Kart, &mut Transform)>,
) {
    for (_, mut transform) in query.iter_mut() {
        let mut net_forward = 0.0;
        let mut rotation = 0.0;

        if keyboard_input.pressed(KeyCode::W) {
            net_forward += 1.0;
        }
        if keyboard_input.pressed(KeyCode::S) {
            net_forward -= 1.0;
        }
        if keyboard_input.pressed(KeyCode::A) {
            rotation += 1.0;
        }
        if keyboard_input.pressed(KeyCode::D) {
            rotation -= 1.0;
        }

        if rotation != 0.0 {
            let delta_time = 0.1; // Can replace this with time.delta_seconds() for smooth movement
            let speed = 1.0;

            transform.rotate(Quat::from_rotation_y(rotation * speed * delta_time));
        }

        if net_forward != 0.0 {
            let delta_time = 0.1; // Can replace this with time.delta_seconds() for smooth movement
            let speed = 3.0;

            let local_z = transform.local_z();
            let translation = transform.translation;
            let new_translation = translation + net_forward * local_z * speed * delta_time;
            transform.translation = new_translation;
        }
    }
}
