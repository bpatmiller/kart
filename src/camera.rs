use crate::kart;
use bevy::prelude::*;

#[derive(Component)]
pub struct ThirdPersonCamera;

pub fn setup_camera(mut commands: Commands) {
    commands
        .spawn(Camera3dBundle {
            transform: Transform::from_xyz(0.0, 5.0, 10.0).looking_at(Vec3::ZERO, Vec3::Y),
            ..Default::default()
        })
        .insert(ThirdPersonCamera);
}

pub fn third_person_camera_system(
    mut queries: ParamSet<(
        Query<(&ThirdPersonCamera, &mut Transform)>,
        Query<(&kart::Kart, &Transform)>,
    )>,
) {
    if let Some((_, kart_transform)) = queries.p1().iter().next() {
        let kart_position = kart_transform.translation;
        let local_z = kart_transform.local_z();
        let target_position = kart_position + Vec3::new(0.0, 2.0, 0.0); // Target slightly above the kart

        for (_, mut camera_transform) in queries.p0().iter_mut() {
            let camera_position = camera_transform.translation;

            // Adjust the distance and height of the camera
            let camera_distance = 7.0;
            let camera_height = 3.0;
            let desired_position =
                kart_position - local_z * camera_distance + Vec3::new(0.0, camera_height, 0.0);

            // Interpolate the camera position for smoother movement
            let camera_speed = 5.0;
            let delta_time = 0.1; // Can replace this with time.delta_seconds() for smooth movement
            let interpolated_position =
                camera_position.lerp(desired_position, camera_speed * delta_time);

            camera_transform.translation = interpolated_position;
            camera_transform.look_at(target_position, Vec3::Y);
        }
    }
}
