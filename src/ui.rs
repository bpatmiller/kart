use wasm_bindgen::prelude::*;

pub fn get_window_dimensions() -> (f32, f32) {
    let window = web_sys::window().unwrap();
    let document = window.document().unwrap();
    let body = document.body().unwrap();
    let width = body.client_width() as f32;
    let height = body.client_height() as f32;
    (width, height)
}

pub fn close_loader() -> Result<(), JsValue> {
    let window = web_sys::window().unwrap();
    let document = window.document().unwrap();
    let loader_wrapper = document.query_selector(".loader-wrapper").unwrap().unwrap();

    let current_classes = loader_wrapper.class_name();
    loader_wrapper.set_class_name(&format!("{} hidden", current_classes));

    Ok(())
}
